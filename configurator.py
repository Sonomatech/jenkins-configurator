import os
import sys
import yaml
import cgi
import html

from string import Template

import pprint

TEMPLATES_DIR = "templates"
OUTPUT_DIR = "build"


pp = pprint.PrettyPrinter(indent=4)

def handle_yaml_constructors(loader, suffix, node):
    """
    Constructor method for PyYaml to handle cfn intrinsic functions specified as yaml tags
    """
    function_mapping = {
        "!and": ("Fn::And", lambda x: x),
        "!base64": ("Fn::Base64", lambda x: x),
        "!condition": ("Condition", lambda x: x),
        "!equals": ("Fn::Equals", lambda x: x),
        "!findinmap": ("Fn::FindInMap", lambda x: x),
        "!if": ("Fn::If", lambda x: x),
        "!importvalue": ("Fn::ImportValue", lambda x: x),
        "!join": ("Fn::Join", lambda x: [x[0], x[1]]),
        "!not": ("Fn::Not", lambda x: x),
        "!or": ("Fn::Or", lambda x: x),
        "!ref": ("Ref", lambda x: x),
        "!select": ("Fn::Select", lambda x: x),
        "!split": ("Fn::Split", lambda x: x),
        "!sub": ("Fn::Sub", lambda x: x),
        "!index": ("Fn::Index", lambda x: x)
    }
    try:
        function, value_transformer = function_mapping[str(suffix).lower()]
    except KeyError as key:
        raise Exception("Unsupported cfn intrinsic function tag found: {0}".format(key))

    if isinstance(node, yaml.ScalarNode):
        value = loader.construct_scalar(node)
    elif isinstance(node, yaml.SequenceNode):
        value = loader.construct_sequence(node)
    elif isinstance(node, yaml.MappingNode):
        value = loader.construct_mapping(node)
    else:
        raise Exception("Invalid yaml node found while handling cfn intrinsic function tags")

    return {function: value_transformer(value)} 

class MyLoader(yaml.SafeLoader):
	pass


yaml.add_multi_constructor(u"", handle_yaml_constructors, Loader=MyLoader)


def parse_config(config_path):
	with open(config_path, 'r') as file:
		config = yaml.load(file.read(), Loader=MyLoader)

		return config

def get_template(template_path):
	with open(os.path.join(TEMPLATES_DIR, template_path), 'r') as file:
		template = Template(file.read())

		return template

def compute_var(var, gobbles):
	if type(var) in (str, float, int):
		return html.escape(str(var))
	
	if type(var) is dict:
		if "Fn::Join" in var:
			sep, items = var["Fn::Join"]
			return sep.join(str(compute_var(item, gobbles)) for item in items)
		elif "Fn::Index" in var:
			return gobbles["index"]

	return var

def main(config_path):
	config = parse_config(config_path)

	for label, job in config["jobs"].items():
		count = job["count"] if "count" in job else 1
		for i in range(count):
			gobbles = {
				"index": i+1
			}

			template = get_template(job["template"])
			computed_vars = dict([(key, compute_var(var, gobbles)) for key, var in job["environment"].items()])

			pp.pprint(computed_vars)

			job_config_body = template.substitute(computed_vars)
			output_path = os.path.join(OUTPUT_DIR, str(compute_var(job["name"], gobbles))+".xml")

			with open(output_path, 'w') as fp:
				fp.write(job_config_body)


if __name__ == "__main__":
	config_path = sys.argv[1]
	main(config_path)