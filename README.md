# Jenkins Configurator

Generates Jenkins configs in a declarative manner by using string templates and YAML config, with syntax shamelessly stolen from AWS Cloudformation.

## Getting Started

Install PyYAML, the sole dependency. From the project directory, run the following:

```
$ pip3 install -t ./site-packages --upgrade pyyaml
```

## Running the Example

The project intially comes with a single template and an example configuration. Copy `production.yaml.example` to `production.yaml` and make any changes as necessary.

From the project directory, run the following:

```
$ PYTHONPATH="$PYTHONPATH:$(pwd -P)/site-packages" python3 configurator.py production.yaml
```

The script will build Jenkins configs based on the YAML config and the associated template file. The resultant Jenkins config will land in the `build` directory.


## Getting the existing configs

ssh into jenkins box

```
$ mkdir jenkins-configs

$ cd jenkins-configs

$ find /var/lib/jenkins/jobs -maxdepth 2 -type f -name config.xml | python -c "import sys; import shutil; items = [shutil.copyfile(line.strip(), str(line.strip().split('/')[5].replace(' ', '_')+'.xml')) for line in sys.stdin];"

$ exit
```

then copy those files down

```
$ scp -r -P 56776 rcastillo@super-mahn.sonomatech.com:~/jenkins-configs/ .
```

## Links

* https://stackoverflow.com/questions/52240554/how-to-parse-yaml-using-pyyaml-if-there-are-within-the-yaml
* https://www.programcreek.com/python/example/15349/yaml.SequenceNode