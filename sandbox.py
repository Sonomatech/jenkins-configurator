import sys
import yaml

import pprint


pp = pprint.PrettyPrinter(indent=4)

class Ref(object):
	def __init__(self, data):
		self.data = data
	def __repr__(self):
		return "Ref(%s)" % self.data

class Index(object):
	def __init__(self):
		pass
	def __repr__(self):
		return "Index"

def create_ref(loader, node):
	value = loader.construct_scalar(node)
	return Ref(value)

def create_index(loader, node):
	return Index()

class MyLoader(yaml.SafeLoader):
	pass

yaml.add_constructor(u'!Ref', create_ref, MyLoader)
yaml.add_constructor(u'!Index', create_index, MyLoader)

config = yaml.load(doc, Loader=MyLoader)

pp.pprint(config)